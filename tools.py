import pandas as pd
import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


def get_best_courses(df, top_n=5, min_viewers=5):
    result = df.groupby("course", as_index=False)["rating"].agg(["mean", "size"])
    result = (result[result["size"] >= min_viewers]
              .sort_values(by="mean", ascending=False)[:top_n]
              .reset_index()[["course", "mean"]])
    
    return result


def get_best_courses_by_industry(df, top_n=5, min_viewers=3):
    
    result = (df
              .groupby(["industry", "course"], as_index=False)["rating"]
              .agg(["mean", "size"]))
    result = result[result["size"] >= min_viewers]

    result["rank"] = result.groupby(["industry"])["mean"].rank("first", ascending=False)
    result = result[result["rank"] <= top_n].reset_index()

    result = result[["course", "industry", "mean"]]

    return result


def add_dummy_variables(df, columns):
    
    dummy_dataset = pd.get_dummies(df, columns=columns)
    
    dummy_columns = [col for col in dummy_dataset.columns if col.startswith(tuple(columns))]
    id_columns = ["username", "course"]
    
    dummy_dataset = dummy_dataset[id_columns + dummy_columns]

    return pd.merge(df, dummy_dataset, on=id_columns)



class PeersFeatureExtractor(BaseEstimator, TransformerMixin):
        
    def _set_course_stat(self, df):
        self.course_stat = df.groupby("course", as_index=False)["rating"].aggregate([min, np.mean, max, len])
        self.course_stat = self.course_stat.rename(columns={"min":  "course_stat_min", 
                                                            "mean": "course_stat_avg", 
                                                            "max":  "course_stat_max", 
                                                            "len":  "course_stat_len"})
        
    
    def _set_industry_stat(self, df):
        self.industry_stat = df.groupby("industry", as_index=False)["rating"].aggregate([min, np.mean, max, len])
        self.industry_stat = self.industry_stat.rename(columns={"mean": "industry_stat_avg", 
                                                                "len":  "industry_stat_len"})

    
    def _add_course_stat(self, df):
        
        df = pd.merge(df, self.course_stat, on="course", how="left")
        return df
    
    
    def _add_industry_stat(self, df):
        
        df = pd.merge(df, self.industry_stat, on="industry", how="left")
        return df
    
    
    def fit(self, df, y=None):
        
        self._set_course_stat(df)
        self._set_industry_stat(df)
        return self
    
    def transform(self, df, y=None) -> pd.DataFrame:
        
        df = self._add_course_stat(df)
        df = self._add_industry_stat(df)
        return df.fillna(0)

    def fit_transform(self, df, y=None) -> pd.DataFrame:
        
        self.fit(df)
        return self.transform(df)
